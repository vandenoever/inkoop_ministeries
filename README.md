# Spending information for Dutch ministries

Spending information is available in CSV form from https://data.overheid.nl/.
This project retrieves all CSV files for the ministries and sums them up.

The result can be seen here: http://vandenoever.gitlab.io/inkoop_ministeries/.

Totals are available for each company.

The information is mirrored in this repository. To update the information, run `make`. This requires `npm`. The information is collected in `public/data.json`. `public/index.html` shows a large table with all information.

Amounts are not exact. Each relation between a ministry and a company falls in t a 'staffel'. A staffel can be '€ 1 – € 10.000' to '€ 150.000.001 – ∞'. That's right, the highest category has no upper limit.

The code to sum up the amounts uses all information in the CSV files.

All amount are direct spending. Many companies in the list also receive indirect spending via another company or organization in the list. The funds that are received indirectly may be much higher than the direct funds. Only information on direct spending is available.

https://data.overheid.nl/data/dataset?tags=Spend
