/*global require, console*/
var fs = require("fs"),
    request = require("request"),
    async = require("async"),
    iconvlite = require('iconv-lite'),
    csvparse = require('csv-parse/lib/sync');


var ministeries = [
        "BZK", "FIN", "VWS", "V&J", "SZW", "OCW",
        "I&M", "EZ", "DEF", "BZ", "AZ"
    ],
    categorien = [
        "Communicatie", "Diensten en middelen", "Dienstspecifiek",
        "Huisvesting", "ICT", "Personele exploitatie", "Uitbesteden",
        "Vervoer en verblijfskosten"
    ],
    staffels = [
        null,
        { min:         1, max:         10000 }, //  1
        { min:     10001, max:         50000 }, //  2
        { min:     50001, max:        100000 }, //  3
        { min:    100001, max:        250000 }, //  4
        { min:    250001, max:        500000 }, //  5
        { min:    500001, max:       1000000 }, //  6
        { min:   1000001, max:       5000000 }, //  7
        { min:   5000001, max:      10000000 }, //  8
        { min:  10000001, max:      25000000 }, //  9
        { min:  25000001, max:      50000000 }, // 10
        { min:  50000001, max:     100000000 }, // 11
        { min: 100000001, max:     150000000 }, // 12
        { min: 150000001, max:         1 / 0 }  // 13
    ],
    base_url = "http://data.overheid.nl/OpenDataSets/spend/";

function getStaffel(staffel) {
    "use strict";
    return staffels[parseInt(staffel, 10)];
}

function make_urls() {
    "use strict";
    var urls = [];
    ministeries.forEach(function (ministry) {
        categorien.forEach(function (category) {
            var file = ministry,
                url;
            // algemene zaken heeft onregelmatige bestandsnamen
            if (ministry === "AZ") {
                if (category === "ICT") {
                    file += " " + category + " 2014.csv";
                } else {
                    file += " " + category.toLowerCase() + " 2014.csv";
                }
            } else {
                file += " 2014 " + category + ".csv";
            }
            url = base_url + encodeURIComponent(ministry) + "/" +
                    encodeURIComponent(file);
            urls.push({
                url: url,
                file: file,
                ministry: ministry
            });
        });
    });
    return urls;
}

function iso_8859_1_buffer_to_string(buffer) {
    "use strict";
    var content = iconvlite.decode(buffer, "ISO-8859-1");
    // unrecognized data is decoded as the unicode replacement character
    // if \uFFFD is present, the buffer was not valid ISO-8859-1
    if (content.indexOf("\uFFFD") !== -1) {
        throw "Bytearray does not match ISO-8859-1 encoding.";
    }
    return content;
}

function getFile(path, csv, callback) {
    "use strict";
    var options = {
            timeout: 5000,
            encoding: null // return a Buffer from the request
        };
    request(csv.url, options, function (error, response, content) {
        if (error) {
            callback("Cannot retrieve " + csv.url + ".");
        } else if (response.statusCode !== 200) {
            callback("Cannot retrieve " + csv.url + ": "
                        + response.statusCode);
        } else {
            fs.writeFileSync(path, content);
            callback(null, content);
        }
    });
}

function readFile(csv, callback) {
    "use strict";
    var path = "data/" + csv.file;
    fs.readFile(path, function (err, content) {
        if (err) {
            getFile(path, csv, callback);
        } else {
            callback(null, content);
        }
    });
}

function mapHeader(data, header) {
    "use strict";
    var i,
        h1,
        h2,
        m,
        n,
        re = /^(\d+) *-? *([\s\S]*)$/,
        map = {};
    for (i = 1; i < header.length; i += 1) {
        m = re.exec(header[i]);
        n = parseInt(m[1], 10);
        h1 = m[2];
        // fix typo in input data
        h1 = h1.replace("beleidsadvies ", "beleidsadvies- ");
        // uppercase the first letter
        h1 = h1.substr(0, 1).toUpperCase() + h1.substr(1);
        h2 = data.categories[n];
        if (h2) {
            if (h1 !== h2) {
                throw "Unequal headers: " + h1 + " " + h2;
            }
        } else {
            data.categories[n] = h1;
        }
        map[i] = n;
    }
    return map;
}

function addLine(leveranciers, ministry, map, line) {
    "use strict";
    var i,
        leverancier = line[0],
        cd = leveranciers[leverancier] || {},
        v;
    if (leverancier.length === 0) {
        return;
    }
    leveranciers[leverancier] = cd;
    cd = cd[ministry] || {};
    leveranciers[leverancier][ministry] = cd;
    for (i = 1; i < line.length; i += 1) {
        v = parseInt(line[i], 10);
        if (v === 13) {
            console.log(leverancier);
        }
        if (v > 0) {
            cd[map[i]] = v;
        }
    }
}

function addData(data, csv, content) {
    "use strict";
    var map = mapHeader(data, content[0]),
        i;
    for (i = 1; i < content.length; i += 1) {
        addLine(data.leveranciers, csv.ministry, map, content[i]);
    }
}

function retrieve_data() {
    "use strict";
    var urls = make_urls(),
        data = {
            staffels: staffels,
            leveranciers: {},
            categories: {}
        };
    async.mapLimit(urls, 1, function (csv, callback) {
        readFile(csv, function (error, content) {
            if (error) {
                callback(error);
            } else {
                try {
                    content = iso_8859_1_buffer_to_string(content);
                    content = csvparse(content, { delimiter: ';' });
                    addData(data, csv, content);
                    csv.content = content;
                    callback(null, csv);
                } catch (e) {
                    callback(e);
                }
            }
        });
    }, function (error) {
        if (error) {
            return console.log(error);
        }
        fs.writeFileSync("public/data.json", JSON.stringify(data, null, " "));
    });
}

retrieve_data();
