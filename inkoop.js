/*global window, alert, XMLHttpRequest, console, document*/

var ministeries = [
        "BZK", "FIN", "VWS", "V&J", "SZW", "OCW",
        "I&M", "EZ", "DEF", "BZ", "AZ"
    ],
    categorien = [
        "Communicatie", "Diensten en middelen", "Dienstspecifiek",
        "Huisvesting", "ICT", "Personele exploitatie", "Uitbesteden",
        "Vervoer en verblijfskosten"
    ],
    files,
    staffels = [
        { min:         0, max:             0 },
        { min:         1, max:         10000 }, //  1
        { min:     10001, max:         50000 }, //  2
        { min:     50001, max:        100000 }, //  3
        { min:    100001, max:        250000 }, //  4
        { min:    250001, max:        500000 }, //  5
        { min:    500001, max:       1000000 }, //  6
        { min:   1000001, max:       5000000 }, //  7
        { min:   5000001, max:      10000000 }, //  8
        { min:  10000001, max:      25000000 }, //  9
        { min:  25000001, max:      50000000 }, // 10
        { min:  50000001, max:     100000000 }, // 11
        { min: 100000001, max:     150000000 }, // 12
        { min: 150000001, max:         1 / 0 }  // 13
    ],
    data = {};

function makeFiles() {
    "use strict";
    files = [];
    ministeries.forEach(function (m) {
        categorien.forEach(function (c) {
            files.push("utf8data/" + m + " 2014 " + c + ".csv");
        });
    });
}
makeFiles();

function getStaffel(staffel) {
    "use strict";
    return staffels[parseInt(staffel, 10)];
}

function clear(e) {
    "use strict";
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
}

function formatEuro(n) {
    "use strict";
    if (!isFinite(n)) {
        return "∞";
    }
    return "€ " + n.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1.');
}

function renderRow(table, category, staffel, className) {
    "use strict";
    var tr = document.createElement("tr"),
        th = document.createElement("td"),
        td = document.createElement("td");
    tr.setAttribute("class", className);
    th.appendChild(document.createTextNode(category));
    tr.appendChild(th);
    if (category.endsWith("totaal")) {
        tr.appendChild(td);
        td = document.createElement("td");
        td.setAttribute("class", "totaal");
    } else {
        td.setAttribute("class", "geld");
    }
    td.appendChild(document.createTextNode(formatEuro(staffel.min)));
    td.appendChild(document.createTextNode(" – "));
    td.appendChild(document.createTextNode(formatEuro(staffel.max)));
    tr.appendChild(td);
    table.appendChild(tr);
}
function keyFromPath(path) {
    "use strict";
    return path.substr(5).split(".csv")[0];
}
function renderCompanyInfo(company, table, beforeElement) {
    "use strict";
    var d = data[company].ministeries,
        fragment = document.createDocumentFragment(),
        m;
    if (!d) {
        return;
    }
    m = Object.keys(d);
    m.sort();
    m.forEach(function (key) {
        var tr = document.createElement("tr"),
            th = document.createElement("th"),
            d2 = d[key],
            keys = Object.keys(d2);
        tr.setAttribute("class", "detail");
        keys.sort();
        th.appendChild(document.createTextNode(key));
        tr.appendChild(th);
        fragment.appendChild(tr);
        keys.forEach(function (k) {
            renderRow(fragment, k, d2[k], "detail");
        });
    });
    renderRow(fragment, "totaal", data[company].totaal, "detail");
    table.insertBefore(fragment, beforeElement);
}
function renderCompany(company) {
    "use strict";
    var div = document.getElementById("company"),
        table = document.createElement("table");
    clear(div);
    renderCompanyInfo(company, table);
    div.appendChild(table);
}

function renderAllTable() {
    "use strict";
    var div = document.getElementById("all"),
        table = document.createElement("table"),
        col,
        companies = Object.keys(data);
    clear(div);
    col = document.createElement("col");
    col.setAttribute("width", "60%");
    table.appendChild(col);
    col = document.createElement("col");
    col.setAttribute("width", "20%");
    table.appendChild(col);
    col = document.createElement("col");
    col.setAttribute("width", "20%");
    table.appendChild(col);
    companies.sort(function (a, b) {
        var sa = data[a].totaal,
            sb = data[b].totaal;
        if (sa.max !== sb.max) {
            return sb.max - sa.max;
        }
        if (sa.min !== sb.min) {
            return sb.min - sa.min;
        }
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    companies.forEach(function (c) {
        renderRow(table, c, data[c].totaal, "samenvatting");
    });
    table.addEventListener("click", function (e) {
        var n = e.target,
            d;
        while (n && n.localName !== "tr") {
            n = n.parentNode;
        }
        if (!n || !n.classList.contains("samenvatting")) {
            return;
        }
        d = n.nextElementSibling;
        if (d && d.classList.contains("detail")) {
            while (d && d.classList.contains("detail")) {
                d = d.nextElementSibling;
                n.parentNode.removeChild(n.nextElementSibling);
            }
        } else {
            renderCompanyInfo(n.firstElementChild.firstChild.data, table,
                    n.nextElementSibling);
        }
    });
    div.appendChild(table);
}

function setCompanyTotaal(company) {
    "use strict";
    var d = data[company].ministeries,
        totaal = { min: 0, max: 0 };
    if (!d) {
        return;
    }
    Object.keys(d).forEach(function (key) {
        var d2 = d[key];
        if (d2 && d2.subtotaal) {
            totaal.min += d2.subtotaal.min;
            totaal.max += d2.subtotaal.max;
        }
    });
    data[company].totaal = totaal;
}

function updateLoading() {
    "use strict";
    var all = document.getElementById("all");
    clear(all);
    all.appendChild(document.createTextNode(files.length));
}

function setCompanies() {
    "use strict";
    files.shift();
    if (files.length) {
        updateLoading();
        return;
    }
    var select = document.getElementById("company_select"),
        next = select.nextSibling,
        parent = select.parentNode,
        companies = Object.keys(data);
    parent.removeChild(select);
    clear(select);
    companies.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    companies.forEach(function (c) {
        var option = document.createElement("option");
        option.appendChild(document.createTextNode(c));
        option.setAttribute("value", c);
        select.appendChild(option);
        setCompanyTotaal(c);
    });
    parent.insertBefore(select, next);
    console.log(companies.length);
    renderAllTable();
}

function parse(path, text) {
    "use strict";
    var lines = text.split("\n"),
        header = lines.shift().split(";");
    lines = lines.map(function (line) {
        return line.split(";");
    });
    lines.forEach(function (fields) {
        var company = fields[0],
            companyData = data[company] || {ministeries: {}},
            key = keyFromPath(path),
            categoryData = companyData.ministeries[key] || {},
            staffel,
            min = 0,
            max = 0,
            i;
        data[company] = companyData;
        companyData.ministeries[key] = categoryData;
        for (i = 1; i + 1 < header.length; i += 1) {
            staffel = getStaffel(fields[i]);
            if (staffel) {
                min += staffel.min;
                max += staffel.max;
                categoryData[header[i]] = staffel;
            }
        }
        staffel = getStaffel(fields[header.length - 1]);
        if (staffel) {
            if (header.length > 2) {
                categoryData.subtotaal = {
                    min: Math.max(staffel.min, min),
                    max: Math.min(staffel.max, max)
                };
            } else {
                categoryData.subtotaal = {
                    min: staffel.min,
                    max: staffel.max
                };
            }
        }
    });
    setCompanies();
}

function loadFile(path) {
    "use strict";
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            parse(path, xhr.responseText);
        }
    };
    xhr.open("GET", path, true);
    xhr.setRequestHeader('Content-type', 'text/plain; charset=utf8');
    xhr.send();
}

window.addEventListener("load", function () {
    "use strict";
    files.forEach(function (path) {
        loadFile(path);
    });
    var select = document.getElementById("company_select"),
        timeout;
    select.addEventListener("change", function () {
        renderCompany(select.value);
    });
    select.addEventListener("keyup", function () {
        if (timeout) {
            window.clearTimeout(timeout);
        }
        timeout = window.setTimeout(function () {
            select.blur();
            select.focus();
        }, 500);
    });
});
