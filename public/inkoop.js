/*global window, alert, XMLHttpRequest, console, document*/

function getStaffel(data, staffel) {
    "use strict";
    return data.staffels[parseInt(staffel, 10)];
}

function clear(e) {
    "use strict";
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
}

function formatEuro(n) {
    "use strict";
    if (!isFinite(n)) {
        return "∞";
    }
    return "€ " + n.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1.');
}

function renderRow(table, category, staffel, className) {
    "use strict";
    var tr = document.createElement("tr"),
        th = document.createElement("td"),
        td = document.createElement("td");
    tr.setAttribute("class", className);
    th.appendChild(document.createTextNode(category));
    tr.appendChild(th);
    if (category === "totaal" || category[1] === " ") {
        tr.appendChild(td);
        td = document.createElement("td");
        td.setAttribute("class", "totaal");
    } else {
        td.setAttribute("class", "geld");
    }
    td.appendChild(document.createTextNode(formatEuro(staffel.min)));
    td.appendChild(document.createTextNode(" – "));
    td.appendChild(document.createTextNode(formatEuro(staffel.max)));
    tr.appendChild(td);
    table.appendChild(tr);
}
function keyFromPath(path) {
    "use strict";
    return path.substr(5).split(".csv")[0];
}
function renderCompanyInfo(data, company, table, beforeElement) {
    "use strict";
    var d = data.leveranciers[company],
        fragment = document.createDocumentFragment(),
        m;
    if (!d) {
        return;
    }
    m = Object.keys(d);
    m.sort();
    m.forEach(function (key) {
        if (key === "totaal") {
            return;
        }
        var tr = document.createElement("tr"),
            th = document.createElement("th"),
            d2 = d[key],
            keys = Object.keys(d2);
        tr.setAttribute("class", "detail");
        keys.sort(function (a, b) {
            // make sure single digit numbers follow the multidigi numbers
            // with the same starting digit
            if (b.indexOf(a) === 0) {
                return 1;
            }
            return a.toLowerCase().localeCompare(b.toLowerCase());
        });
        th.appendChild(document.createTextNode(key));
        tr.appendChild(th);
        fragment.appendChild(tr);
        keys.forEach(function (k) {
            var n = k + " - " + data.categories[k];
            renderRow(fragment, n, d2[k], "detail");
        });
    });
    renderRow(fragment, "totaal", d.totaal, "detail");
    table.insertBefore(fragment, beforeElement);
}
function renderAllTable(data) {
    "use strict";
    var div = document.getElementById("all"),
        table = document.createElement("table"),
        col,
        l = data.leveranciers,
        companies = Object.keys(l);
    clear(div);
    col = document.createElement("col");
    col.setAttribute("width", "60%");
    table.appendChild(col);
    col = document.createElement("col");
    col.setAttribute("width", "20%");
    table.appendChild(col);
    col = document.createElement("col");
    col.setAttribute("width", "20%");
    table.appendChild(col);
    companies.sort(function (a, b) {
        var sa = l[a].totaal,
            sb = l[b].totaal;
        if (sa.max !== sb.max) {
            return sb.max - sa.max;
        }
        if (sa.min !== sb.min) {
            return sb.min - sa.min;
        }
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    companies.forEach(function (c) {
        renderRow(table, c, l[c].totaal, "samenvatting");
    });
    table.addEventListener("click", function (e) {
        var n = e.target,
            d;
        while (n && n.localName !== "tr") {
            n = n.parentNode;
        }
        if (!n || !n.classList.contains("samenvatting")) {
            return;
        }
        d = n.nextElementSibling;
        if (d && d.classList.contains("detail")) {
            while (d && d.classList.contains("detail")) {
                d = d.nextElementSibling;
                n.parentNode.removeChild(n.nextElementSibling);
            }
        } else {
            renderCompanyInfo(data, n.firstElementChild.firstChild.data, table,
                    n.nextElementSibling);
        }
    });
    div.appendChild(table);
}

function getLeverancierMinisterieTotal(staffels, categories) {
    "use strict";
    var categorie,
        i,
        subtotals = [],
        subtotal,
        staffel,
        total;
    // sum subcategories
    for (categorie in categories) {
        if (categories.hasOwnProperty(categorie)) {
            i = parseInt(categorie.substr(0, 1), 10);
            staffel = staffels[categories[categorie]];
            if (categorie.length > 1 && staffel) { // subcategorie
                categories[categorie] = staffel;
                subtotal = subtotals[i];
                if (subtotal) {
                    subtotal.min += staffel.min;
                    subtotal.max += staffel.max;
                } else {
                    subtotal = { min: staffel.min, max: staffel.max };
                    subtotals[i] = subtotal;
                }
            }
        }
    }
    // set subtotals
    for (categorie in categories) {
        if (categories.hasOwnProperty(categorie)) {
            if (categorie.length === 1) { // categorie
                staffel = staffels[categories[categorie]];
                i = parseInt(categorie.substr(0, 1), 10);
                subtotal = subtotals[i];
                if (subtotal) {
                    subtotal.min = Math.max(staffel.min, subtotal.min);
                    subtotal.max = Math.min(staffel.max, subtotal.max);
                } else {
                    subtotal = { min: staffel.min, max: staffel.max };
                }
                categories[categorie] = subtotal;
                if (total) {
                    total.min += subtotal.min;
                    total.max += subtotal.max;
                } else {
                    total = { min: subtotal.min, max: subtotal.max };
                }
            }
        }
    }
    return total;
}

function init(data) {
    "use strict";
    console.log("init");
    var staffels = data.staffels,
        leveranciers = data.leveranciers,
        leverancier,
        ministerie,
        m,
        total,
        t;
    // link staffels into the data
    for (leverancier in leveranciers) {
        if (leveranciers.hasOwnProperty(leverancier)) {
            total = undefined;
            m = leveranciers[leverancier];
            for (ministerie in m) {
                if (m.hasOwnProperty(ministerie)) {
                    t = getLeverancierMinisterieTotal(staffels, m[ministerie]);
                    if (total) {
                        total.min += t.min;
                        total.max += t.max;
                    } else {
                        total = { min: t.min, max: t.max };
                    }
                }
            }
            m.totaal = total;
        }
    }
    renderAllTable(data);
}

function loadFile(path) {
    "use strict";
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        var data;
        if (xhr.readyState === 4 && xhr.status === 200) {
            data = JSON.parse(xhr.responseText);
            data.staffels[13].max = 1 / 0; // JSON does not allow Infinity
            init(data);
        }
    };
    xhr.open("GET", path, true);
    xhr.setRequestHeader('Content-type', 'text/plain; charset=utf8');
    xhr.send();
}

window.addEventListener("load", function () {
    "use strict";
    loadFile("data.json");
});
